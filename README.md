-----------------------------------------------------------------------------------------------------------------------------
# PyProb: A Python library of Probabilistic Graphical Models
-----------------------------------------------------------------------------------------------------------------------------
> Based on Kevin Murphy's [BNT](https://github.com/bayesnet/bnt)

*Author*: Jesús Rodríguez Pérez  
*Contact*: rguezperezjesus[at]gmail.com

## Current (stable) features

* Representation
    * Discrete variables / Tabular CPDs
    * Models
        * Bayesian Networks (BNs)
        * Markov Random Fields (MRFs) / Markov networks
        * Dynamic/Temporal Bayesian Networks (DBNs)
            * Implements Hidden Markov Models (e.g. Knowledge Tracing)

* Parameter estimation from both complete and incomplete data                       

* Inference engines
    * Exact
        * Junction Tree algorithm for BNs and MRFs
        * Junction Tree algorithm for DBNs
            * Offline (unrolled DBN)
            * Online (2-timeslice BN)


## Prospective features

* Continuous variables / Gaussian CPDs

* Inference engines
    * Approximate
        * Loopy Belief Propagation for BNs and MRFs


## Usage

Install dependencies* from PyPi:

    pip install -r requirements.txt

*Supports Python 2.7

For testing, from the directory */Tests* run:
    
    nosetests -w ..

See the [Documentation](#Documentation)


# Documentation