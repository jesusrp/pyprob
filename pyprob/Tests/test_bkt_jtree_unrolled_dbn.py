""" Test BKT learning

BKT is implemented as a Dynamic Bayesian Network. The DBN is to be unrolled for N time slices and inference/learning
is performed in the unrolled static network with conditional PDFs shared across time slices.

K1 _ K2 ... KN
 |    |      |
C1   C2 ... CN

"""

# Imports
from unittest import TestCase

import numpy as np

from pyprob import models, cpds, inference, utilities


class Test(TestCase):
    def test_bkt_jtree_unrolled_dbn(self):
        """
        Testing: BKT
        """

        # HMM parameters
        # prior
        pi = np.array([0.8, 0.2])  # Initial p(K0)=0.2

        # state emission probabilities matrix (column vectors)
        B = np.array([[0.7, 0.3],  # Initial p(G)=0.3
                      [0.3, 0.7]])  # Initial p(S)=0.3

        # state transition probabilities matrix (column vectors)
        A = np.array([[0.2, 0.8],  # Initial p(T)=0.8
                      [0, 1]])

        # DBN
        intra = np.array([[0, 1], [0, 0]])  # Intra-slice dependencies
        inter = np.array([[1, 0], [0, 0]])  # Inter-slice dependencies

        node_sizes = np.array([2, 2])

        discrete_nodes = [1, 2]
        continuous_nodes = None

        node_cpds = [cpds.TabularCPD(pi),
                     cpds.TabularCPD(B),
                     cpds.TabularCPD(A)]

        dbn = models.DBN(intra, inter, node_sizes, discrete_nodes,
                         continuous_nodes, node_cpds)

        inference_engine = inference.JTreeUnrolledDBNInferenceEngine()
        inference_engine.model = dbn

        inference_engine.initialize(T=3)
        dbn.inference_engine = inference_engine

        # Test learning
        skill_to_student_to_data = utilities.read_db_samples('student_samples.txt',
                                                   keys=["skill_id", "student_id"],
                                                   values=["knowledge", "correct"], dtype='int')

        for skill in skill_to_student_to_data.keys():
            samples = [student[1] for student in skill_to_student_to_data[skill].items()]
            dbn.learn_params_EM(samples, max_iter=10, thresh=1e-4)

        self.assertAlmostEqual(dbn.node_cpds[0].CPT[1], 0.349452, 6)        # p(K0) = 0.349452...
        self.assertAlmostEqual(dbn.node_cpds[1].CPT[0][1], 0.886139, 6)     # p(G) = 0.886139...
        self.assertAlmostEqual(dbn.node_cpds[1].CPT[1][0], 0.230122, 6)     # p(S) = 0.230122...
        self.assertAlmostEqual(dbn.node_cpds[2].CPT[0][1], 0.832937, 6)     # p(T) = 0.832937...
