__author__ = "jesusrp"

"""Import the required numerical modules"""
import numpy as np

"""Import the pyprob modules"""
from pyprob import models
from pyprob import cliques
from unittest import TestCase
from pyprob.utilities import read_samples


class Test(TestCase):
    def test_mrf_maxsum(self):
        """
        Testing: MAX-SUM of MRF
        This example is based on the lawn sprinkler example, and the Markov
        random field has the following structure.

                                Cloudy - 0
                                 /  \
                                /    \
                               /      \
                       Sprinkler - 1  Rainy - 2
                               \      /
                                \    /
                                 \  /
                               Wet Grass -3
        """
        """Assign a unique numerical identifier to each node"""
        C = 0
        S = 1
        R = 2
        W = 3

        """Assign the number of nodes in the graph"""
        nodes = 4

        """
        The graph structure is represented as a adjacency matrix, dag.
        If adj_mat[i, j] = 1, then there exists a undirected edge from node
        i and node j.
        """
        adj_mat = np.matrix(np.zeros((nodes, nodes), dtype=int))
        adj_mat[C, [R, S]] = 1
        adj_mat[R, W] = 1
        adj_mat[S, W] = 1

        """
        Define the size of each node, which is the number of different values a
        node could observed at. For example, if a node is either True of False,
        it has only 2 possible values it could be, therefore its size is 2. All
        the nodes in this graph has a size 2.
        """
        ns = 2 * np.ones(nodes, dtype=int)

        """
        Define the clique domains. The domain of a clique, is the indices of the
        nodes in the clique. A clique is a fully connected set of nodes.
        Therefore, for a set of node to be a clique, every node in the set must
        be connected to every other node in the set.
        """
        clq_doms = [[0, 1, 2], [1, 2, 3]]

        """Define potentials for the cliques"""
        clqs = []
        T = np.zeros((2, 2, 2))
        T[:, :, 0] = np.array([[0.2, 0.2], [0.09, 0.01]])
        T[:, :, 1] = np.array([[0.05, 0.05], [0.36, 0.04]])
        clqs.append(cliques.Clique(0, clq_doms[0], np.array([2, 2, 2]), T))
        T[:, :, 0] = np.array([[1, 0.1], [0.1, 0.01]])
        T[:, :, 1] = np.array([[0, 0.9], [0.9, 0.99]])
        clqs.append(cliques.Clique(1, clq_doms[1], np.array([2, 2, 2]), T))

        """Create the MRF"""
        net = models.MRF(adj_mat, ns, clqs, lattice=False)

        """
        Intialize the MRF's inference engine to use EXACT inference, by
        setting exact=True.
        """
        net.init_inference_engine(exact=True)

        """Create and enter evidence ([] means that node is unobserved)"""
        all_ev = read_samples('sprinkler_ev.txt', dtype='int')
        all_mpe = read_samples('sprinkler_mpe.txt', dtype='int')

        count = 0
        errors = 0
        for evidence in all_ev:
            """Execute the max-sum algorithm"""
            mlc = net.max_sum(evidence)

            mpe = all_mpe[count]
            errors = errors + np.sum(np.array(mpe) - np.array(mlc))

            count += 1

        self.assertEquals(errors, 0)
