__author__ = "jesusrp"
"""
This module contains the classes used to perform inference on various
graphical models.
"""

import numpy as np

import general
import graph
import cliques
import models


class InferenceEngine(object):
    """ Interface for inference engines. 
    
    Allows different kinds of inference engines to
    be easily swapped in a model.

    @DynamicAttrs
    """

    def __init__(self, model=None):
        self._model = model

    def get_model(self):
        """ Return model """
        return self._model

    def set_model(self, model):
        """ Validate and set model """
        self.validate(model)
        self._model = model

    def validate(self, model):
        """ Default implementation does nothing """
        pass

    model = property(get_model, set_model)


class JTreeInferenceEngine(InferenceEngine):
    """ Base class for exact juntion tree inference engines.

    @DynamicAttrs
    """

    def __init__(self, model=None, clusters=list()):
        """
        Intializes the inference engine.

        Parameters
        ----------
        Model: Either a MRF or BNET object (models.py)
            The model to perfrom infernece on.
        """
        """
        Initialize the engine for the specific model it is being assigned to.
        """
        # Call init method of super class 
        InferenceEngine.__init__(self, model)

        self.cliques_bitv = None
        self.clique_weight = None
        self.tri_cliques = None

        if model is None:
            return
        if type(model) is models.MRF:
            self.init_for_mrf(model)
        elif type(model) is models.BNET:
            self.init_for_bnet(model, clusters)
        else:
            raise AttributeError("Model not supported")

        self.node_pots = []

    def init_for_mrf(self, mrf):
        """
        Initializes a junction tree inference engine for specific 
        markov random field. The process in this function is known as the
        'Compilation phase' of the junction tree algorithm.

        Parameters
        ----------
        mrf: mrf object
            The markov random field to perform inference on.
        """
        """Initialize all the data members"""
        self.jtree = []
        self.model = mrf

        """
        Now perform triagulation and obtain the junction tree from the input
        undirected graph.
        """
        [self.jtree, root, clq_doms, B, w] = \
            graph.graph_to_jtree(self.model.adj_mat,
                                 self.model.node_sizes)

        """
        Store the clique bitvector, which is used to lookup which nodes
        are in which cliques, and store the clique weights, of how many
        possible configuarations the nodes within each clique can assume.
        """
        self.cliques_bitv = B
        self.clique_weight = w

        """
        Create a set of discrete cliques representing the cliques in the
        triangulated graph.
        """
        self.tri_cliques = []
        count = 0
        for clq_dom in clq_doms:
            """Create a clique with a blank potential"""
            clq_dom = np.array(clq_dom, dtype=int).tolist()
            self.tri_cliques.append(cliques.Clique(count, clq_dom[:],
                                                   self.model.node_sizes[clq_dom]))

            """
            Now create the new potential for the triangulated clique
            by combining the input clique potentials that it consists of.
            """
            tri_pot = self.tri_cliques[-1].unobserved_pot
            for clique in self.model.cliques:
                """
                Determine if this original clique can contribute to the
                new clique.
                """
                if len(clique.domain) < len(clq_dom):
                    inter = general.myintersect(np.array(clique.domain),
                                                np.array(clq_dom), stype='natural')
                else:
                    inter = general.myintersect(np.array(clq_dom),
                                                np.array(clique.domain), stype='natural')

                """
                If there is an intersection, incorporate the required
                information from this clique to the new clique.
                """
                if len(inter) == len(clique.domain):
                    """
                    Make a copy of the original potential and marginalize out
                    the unrequired nodes.
                    """
                    temp_pot = clique.unobserved_pot.marginalize_pot(inter)[0]

                    """
                    Multiply the marginalized original potential into the
                    new triangulated potential.
                    """
                    tri_pot.arithmetic(temp_pot, '*')
            count = count + 1

        """
        Now find the seperation set between all the cliques, start by finding
        the indices of all the edges in the jtree graph matrix.
        """
        xs = []
        ys = []
        for x in range(0, self.jtree.shape[0]):
            temp = graph.find(np.array([self.jtree[x, :]]) > 0)
            if temp.shape[1] != 0:
                xs.append(x)
                ys.append(temp[0, :].tolist())

        """Determine the intersection between each set of connected cliques"""
        for x in xs:
            for y in ys[x]:
                """
                Determine the seperating domain between clique x and clique y.
                """
                sep_dom = graph.find(np.array([((B[x, :] + B[y, :])
                                                == 2)])).tolist()[0]

                """
                Clique x is seperated from clique y by the nodes indexed in
                sep_dom.
                """
                self.tri_cliques[x].nbrs[y] = [sep_dom, None]

        """
        Now that we have the the jtree, we convert it to a directed jtree
        pointing away from its root. This is done using a depth-first search,
        and the pre and post visting order obtained from the DFS is used in the
        progagation phase of the algorithm.
        """
        rooted_tree = graph.mk_rooted_tree(self.jtree, len(self.tri_cliques) - 1)
        self.jtree = rooted_tree[0]
        self.preorder = rooted_tree[1]
        self.postorder = rooted_tree[2]

        self.postorder_parents = []
        for n in range(0, len(self.postorder)):
            self.postorder_parents.insert(int(n),
                                          (graph.parents(self.jtree, n)))

        self.preorder_children = []
        for n in range(0, len(self.preorder)):
            self.preorder_children.insert(int(n),
                                          (graph.children(self.jtree, n)))

    def init_for_bnet(self, bnet, clusters=list()):
        """
        Initializes a junction tree inference engine for specific 
        bayesian network. The process in this function is known as the
        'Compilation phase' of the junction tree algorithm.

        Parameters
        ----------
        bnet: bnet object
            The bayesian network to perform inference on.
        """
        """Initialize all the data members"""
        self.jtree = []
        self.model = bnet

        ns = bnet.node_sizes
        ns[bnet.observed_nodes] = 1
        """Obtained the moralized verion of the directed acyclic graph"""
        self.adj_mat = graph.moralize(bnet.adj_mat)[0]

        """
        Now perform triangulation and obtain the junction tree from the
        graph moralized graph.
        """
        [self.jtree, root, clq_doms, B, w] = \
            graph.graph_to_jtree(self.adj_mat,
                                 ns, [range(len(self.model.adj_mat))], clusters)
        """
        Store the clique bitvector, which is used to lookup which nodes
        are in which cliques, and store the clique weights, of how many
        possible configuarations the nodes within each clique can assume.
        """
        self.clq_doms = clq_doms
        self.cliques_bitv = B
        self.clique_weight = w

        """
        A node can be a member of many cliques, but is assigned to exactly one,
        to avoid double-counting its CPD. We assign node i to clique c if c is
        the "lightest" clique that contains i's family, so it can accomodate
        its CPD.
        """
        self.clq_ass_to_node = np.zeros((1, self.model.num_nodes), dtype=int)
        for i in range(0, self.model.num_nodes):
            """Find the domain this node belongs in"""
            f = graph.family(self.model.adj_mat, i)

            """Find which cliques its domain is a subset of"""
            clqs_containing_family = graph.find(np.array(
                [np.all(B[:, f] == 1, 1)]) == 1)

            """Determine which of the discovered cliques are the lightest"""
            # TODO
            # what is the appropriate weight of continuous nodes? This is a 
            # temporary HACK that sets them to 1 (assuming continuous nodes
            # are observed).
            _w = w.copy()
            _w[_w == np.inf] = 1
            c = clqs_containing_family[0, np.argmin(_w[ \
                                                        clqs_containing_family.tolist(), 0])]

            """Assign the node to a clique"""
            self.clq_ass_to_node[0, i] = c

        self.clq_ass_to_node = self.clq_ass_to_node.squeeze().tolist()
        self.jtree_moral = self.jtree

        """
        Now that we have the the jtree, we convert it to a directed jtree
        pointing away from its root. This is done using a depth-first search,
        and the pre and post visting order obtained from the DFS is used in the
        progagation phase of the algorithm.
        """
        rooted_tree = graph.mk_rooted_tree(self.jtree, len(self.clq_doms) - 1)
        self.jtree = rooted_tree[0]
        self.preorder = rooted_tree[1]
        self.postorder = rooted_tree[2]

        self.postorder_parents = []
        for n in range(0, len(self.postorder)):
            self.postorder_parents.insert(int(n),
                                          (graph.parents(self.jtree, n)))

        self.preorder_children = []
        for n in range(0, len(self.preorder)):
            self.preorder_children.insert(int(n),
                                          (graph.children(self.jtree, n)))

    def update(self):
        """
        Create clique potentials. Noramally called when evidence is entered
        into inference engine.
        Create a set of discrete cliques representing the cliques in the
        triangulated graph. Start by converting all the CPD's to
        potentials.
        """
        # The potentials will now have changes so we reconstruct them.
        self.node_pots = []
        for i in range(0, self.model.num_nodes):
            """Determine the domain of the new clique potential"""
            domain = graph.family(self.model.adj_mat, i)

            """Create the clique potentials from the CPD"""
            cpd = self.model.node_cpds[i]
            # This line enters evidence into the potential, remember to alter
            # the clique node sizes to reflect that.
            node_pot = cpd.convert_to_pot(domain, self.model, self.model.evidence)
            self.node_pots.append(node_pot)

        count = 0
        self.tri_cliques = []
        for clq_dom in self.clq_doms:
            """Create a clique with a blank potential"""
            clq_dom = np.array(clq_dom, dtype=int)
            clq_node_sizes = self.model.node_sizes[clq_dom]

            self.tri_cliques.append(cliques.Clique(count, clq_dom[:],
                                                   clq_node_sizes))

            """
            Now create the new potential for the triangulated clique
            by combining the input clique potentials that it consists of.
            """
            tri_pot = self.tri_cliques[-1].unobserved_pot
            node_ass = self.clq_ass_to_node[:]
            while count in node_ass:
                ndx = node_ass.index(count)
                tri_pot.arithmetic(self.node_pots[ndx], '*')
                node_ass[ndx] = -1
            count = count + 1

            # More mixed arrays/lists, need to stabilize this mismatch
            tri_pot.observed_domain = np.array(self.model.observed_domain)

            # Dont forget to set the 'working' potential to equal the
            # unobserved potential.
            self.tri_cliques[-1].pot = \
                self.tri_cliques[-1].unobserved_pot.copy()

        """
        Now find the seperation set between all the cliques, start by finding
        the indices of all the edges in the jtree graph matrix.
        """
        xs = []
        ys = []
        for x in range(0, self.jtree_moral.shape[0]):
            temp = graph.find(np.array([self.jtree_moral[x, :]]) > 0)
            if temp.shape[1] != 0:
                xs.append(x)
                ys.append(temp[0, :].tolist())

        """Determine the intersection between each set of connected cliques"""
        B = self.cliques_bitv
        for x in xs:
            for y in ys[x]:
                """
                Determine the seperating domain between clique x and clique y.
                """
                sep_dom = graph.find(np.array([((B[x, :] + B[y, :])
                                                == 2)])).tolist()[0]

                """Determine which nodes are observed"""

                """
                Clique x is seperated from clique y by the nodes indexed in
                sep_dom.
                """
                self.tri_cliques[x].nbrs[y] = [sep_dom, None]

        """
        Enter the evidence into the clique potentials, and prepare its
        seperator potentials.
        """
        for clique in self.tri_cliques:
            """Make a copy of the unobserved clique potential to work with"""

            """Initialize this cliques seperator potentials"""
            clique.init_sep_pots(self.model.node_sizes, self.model.observed_domain)

    def sum_product(self, evidence=None):
        """
        Execute the propagation phase of the sum-product algortihm.
        """
        """Determine which nodes are observed"""
        if type(self.model) is models.MRF:
            [hnodes, onodes] = general.determine_observed(evidence)

            """
            Enter the evidence into the clique potentials, and prepare its
            seperator potentials.
            """
            for clique in self.tri_cliques:
                """Make a copy of the unobserved clique potential to work with"""
                clique.pot = clique.unobserved_pot.copy()

                """Enter the evidence into this clique"""
                clique.enter_evidence(evidence)

                """Initialize this cliques seperator potentials"""
                clique.init_sep_pots(self.model.node_sizes.copy(), onodes, False)

        """
        Begin propagation by sending messages from the leaves to the root of the
        jtree, updating the clique and seperator potentials.
        """
        for n in self.postorder:
            n = int(n)
            for p in self.postorder_parents[n]:
                """
                Get handles to neighbouring cliques for easy readability.
                """
                clique_n = self.tri_cliques[n]
                clique_p = self.tri_cliques[p]

                """
                Send message from clique n to variable nodes seperating
                clique n and clique p.
                """
                clique_n.nbrs[p][1] = clique_n.pot.marginalize_pot(
                    clique_n.nbrs[p][0], False)[0]

                clique_p.nbrs[n][1] = clique_n.pot.marginalize_pot(
                    clique_n.nbrs[p][0], False)[0]

                """Send message from variable nodes to clique p"""
                clique_p.pot.arithmetic(clique_n.nbrs[p][1], '*')

        """
        The jtree is now in an incosistant state, restore consistancy and
        complete the propagation phase by sending messages from the root to the
        leaves of the jtree, updating the clique and seperator potentials.
        """
        for n in self.preorder:
            n = int(n)
            for c in self.preorder_children[n]:
                """
                Get handles to neighbouring cliques for easy readability.
                """
                clique_n = self.tri_cliques[n]
                clique_c = self.tri_cliques[c]

                """
                Before sending a message from clique n to clique c, divide
                out the old message from clique n to clique c
                """

                clique_c.pot.arithmetic(clique_c.nbrs[n][1], '/')

                """
                Send message from clique n to variable nodes seperating
                clique n and clique c.
                """
                clique_n.nbrs[c][1] = clique_n.pot.marginalize_pot(
                    clique_n.nbrs[c][0], False)[0]

                """Send message from variable nodes to clique c"""

                clique_c.pot.arithmetic(clique_n.nbrs[c][1], '*')

        """Normalize the clique potentials"""
        loglik = 0
        for clique in self.tri_cliques:
            loglik = clique.pot.normalize_pot()

        return loglik

    def max_sum(self, evidence=None):
        """
        Execute the propagation phase of the max_sum algortihm.

        Parameters
        ----------
        evidence: List
            A list of any observed evidence. If evidence[i] = [], then
            node i is unobserved (hidden node), else if evidence[i] =
            SomeValue then, node i has been observed as being SomeValue.
        """
        """Determine which nodes are observed"""
        evidence = self.model.evidence
        [hnodes, onodes] = general.determine_observed(evidence)

        if type(self.model) is models.MRF:
            """
            Enter the evidence into the clique potentials, and prepare its
            seperator potentials.
            """
            for clique in self.tri_cliques:
                """Enter the evidence into this clique, and log its potential"""
                clique.enter_evidence(evidence, True)

                """Initialize this cliques seperator potentials"""
                clique.init_sep_pots(self.model.node_sizes.copy(), onodes, True)
        else:
            np.seterr(divide='ignore')
            for clique in self.tri_cliques:
                """Enter the evidence into this clique, and log its potential"""
                clique.pot.T = np.log(clique.pot.T)
            np.seterr(divide='warn')

        """Check for special case where there is only one clique"""
        if len(self.tri_cliques) == 1:
            """
            If there is only one clique, we simply find the maximum of that
            cliques potential.
            """
            mlc = self.model.evidence
            max_track = np.argwhere(self.tri_cliques[0].pot.T == \
                                    np.max(self.tri_cliques[0].pot.T))[0]

            """Assign the maximimizing values to the nodes"""
            for i in range(0, self.model.num_nodes):
                if mlc[i] is None:
                    mlc[i] = max_track[i]
        else:
            """
            Initialize maximizing tracker, used to perform back tracking once the
            algorithm has converged.
            """
            max_track = dict()

            """
            Begin propagation by sending messages from the leaves to the root of the
            jtree, updating the clique and seperator potentials.
            """
            order = []
            clique_p = None
            for n in self.postorder:
                n = int(n)
                for p in self.postorder_parents[n]:
                    """
                    Get handles to neighbouring cliques for easy readability.
                    """
                    clique_n = self.tri_cliques[n]
                    clique_p = self.tri_cliques[p]

                    """
                    Send message from clique n to variable nodes seperating
                    clique n and clique p.
                    """
                    ans = clique_n.pot.marginalize_pot(clique_n.nbrs[p][0], True)
                    clique_n.nbrs[p][1] = ans[0]
                    for key in ans[1]:
                        if key not in order:
                            order.insert(0, key)
                        max_track[key] = ans[1][key]

                    """Send message from variable nodes to clique p"""
                    clique_p.pot.arithmetic(clique_n.nbrs[p][1], '+')

            """
            Find the maximum values of the nodes associated with the root
            clique and set them at those values.
            """
            m = np.max(clique_p.pot.T)
            root_max = np.argwhere(clique_p.pot.T == m).squeeze()
            mlc = evidence[:]

            for i in range(0, len(clique_p.domain)):
                if mlc[clique_p.domain[i]] is None:
                    mlc[clique_p.domain[i]] = root_max[i]

            """
            Perform backtracking by traversing back through the order, and
            setting the MAP values of each unobserved node along the way. 
            """
            for node in order:
                if mlc[node] is None:
                    dependants = max_track[node][0]
                    argmax = max_track[node][1]
                    ndx = []
                    for dependant in dependants:
                        ndx.append(mlc[dependant])
                    if len(ndx) != 0:
                        mlc[node] = argmax[tuple(ndx)]
                    else:
                        mlc[node] = argmax

        return mlc

    def marginal_nodes(self, query, maximize=False):
        """
        Marginalize a set of nodes out of a clique.

        Parameters
        ----------
        query: List
            A list of the indices of the nodes to marginalize onto. This set
            of nodes must be a subset of one of the triangulated cliques.

        maximize: Bool
            This value is set to true if we wish to maximize instead of
            marginalize, and False otherwise.
        """
        """Determine which clique, if any, contains the nodes in query"""
        c = self.clq_containing_nodes(query)
        if c == -1:
            print 'There is no clique containing node(s): ', query
            return None
        else:
            """Perform marginalization"""
            m = self.tri_cliques[c].pot.marginalize_pot(query, maximize)[0]
            m = marginal(m.domain, m.T)

        return m

    def marginal_family(self, query):
        """
        """
        """Get the family of the query node"""
        fam = graph.family(self.model.adj_mat, query)

        """Determine which clique the query node was assigned to"""
        c = int(self.clq_ass_to_node[query])

        """Marginalize the clique onto the domain of the family"""
        m = self.tri_cliques[c].pot.marginalize_pot(fam)[0]
        m = marginal(m.domain, m.T)

        return m

    def clq_containing_nodes(self, query):
        """
        Finds the lightest clique (if any) that contains the set of nodes.
        Returns c = -1 if there is no such clique.

        Parameters
        ----------
        query: List
            A list of node the indices we wish to find the containing
            clique of.
        """
        B = self.cliques_bitv
        w = self.clique_weight
        clqs = graph.find(np.array([np.all(B[:, query] == 1, 1)]) == 1)
        c = clqs[0, np.argmin(w[clqs, 0])]

        return int(c)


class JTreeUnrolledDBNInferenceEngine(JTreeInferenceEngine):
    """ Junction tree infereence engine for DBN 

    NOTE: Assumes fixed-length observation sequences
    
    Works by unrolling the DBN into T time slices and applying the
    junction tree algorithm on the resulting static Bayesian network.
    """

    def __init__(self, model=None):
        super(JTreeUnrolledDBNInferenceEngine, self).__init__(model)
        self.T = None

    def initialize(self, T):

        # Unroll dynamic BN into a T time-slice static BN.
        self.bn = self.model.unroll(T)
        self.bn.init_inference_engine()

    def validate(self, model):
        super(JTreeUnrolledDBNInferenceEngine, self).validate(model)

    def marginal_family(self, query):
        return self.bn.engine.marginal_family(query)


class JTree2TBNInferenceEngine(JTreeInferenceEngine):
    """ Online Junction tree inference algorithm for DBNs.
    The same nodes must be observed in every slice.
    This uses the forwards interface of slice t-1 plus all of slice t.
    """

    def __init__(self, model=None):
        super(JTree2TBNInferenceEngine, self).__init__(model)
        self.interface = None

    def initialize(self):
        if self.model is None:
            raise AttributeError("Attribute 'model' not set")

        slice_size = self.model.intra_N

        # Compute the interface of the dynamic BN
        interface = self.model.compute_fwd_interface(slice_size)

        # Convert dynamic BN into a slice-and-half BN.
        self.bn = self.model.mk_slice_and_half(interface, slice_size)
        self.bn.init_inference_engine(clusters=[interface, (np.array(interface) + slice_size).tolist()])

    def enter_evidence(self, evidence=list()):
        """ Enter evidence into the inference engine """
        self.bn.engine.enter_evidence()

    def validate(self, model):
        super(JTree2TBNInferenceEngine, self).validate(model)
        if not type(model) == models.DBN:
            raise TypeError("Class '%s' requires a model of type 'DBN'" % self.__class__.__name__)

    def marginal_family(self, query):
        return self.bn.engine.marginal_family(query)


class marginal(object):
    """
    Stores information about a marginalized node, or nodes.
    """

    def __init__(self, domain, T, mu=list(), sigma=list()):
        self.domain = domain
        self.T = T
        self.mu = mu
        self.sigma = sigma

    def add_ev_to_dmarginal(self, evidence, ns):
        """
        This function 'pumps up' observed nodes back to their original size,
        by introducing zeros into the array positions which are incompatible
        with the evidence.

        Parameters
        ----------
        evidence: List
            A list of any observed evidence. If evidence[i] = [], then
            node i is unobserved (hidden node), else if evidence[i] =
            SomeValue then, node i has been observed as being SomeValue.

        ns: List
            A list of the node sizes of each node in the network.
        """
        dom = self.domain
        odom = []
        equiv = []
        vals = []
        for i in xrange(0, len(dom)):
            if evidence[dom[i]] is not None:
                odom.append(dom[i])
                equiv.append(i)
                vals.append(evidence[dom[i]])

        index = general.mk_multi_index(len(dom), equiv, vals)
        T = np.zeros((ns[dom]))
        ens = ns.copy()
        ens[odom] = 1
        T[index] = np.reshape(self.T, ens[dom])
        self.T = T
