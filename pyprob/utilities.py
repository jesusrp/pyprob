__author__ = "jesusrp"

import csv
import re
import os
from collections import defaultdict

import numpy as np


def read_samples(fname, dtype='float'):
    """
    This function reads a set of values from a text file into a numpy array.

    Parameters
    ----------
    fname: String
        The path to the text file containing the values
    """
    cwd = os.getcwd()
    f = open(cwd+'/Tests/Data/' + fname, 'r')
    lines = f.readlines()
    interactions = lines[1:]
    ans = []

    if dtype != 'int':
        for interaction in interactions:
            tokens = re.split(',', interaction.strip('\n'))
            temp = []
            for val in tokens:
                if val == 'None':
                    temp.append(None)
                else:
                    temp.append(float(val))
            ans.append(temp)
    else:
        for interaction in interactions:
            tokens = re.split(',', interaction.strip('\n'))
            temp = []
            for val in tokens:
                if val == 'None':
                    temp.append(None)
                else:
                    temp.append(int(val))
            ans.append(temp)

    f.close()

    return np.array(ans)


def read_db_samples(fname, keys, values, dtype='float'):
    """
    This function reads a set of values from a comma-separated-value file into a dictionary of dictionaries with shallow
    and deep keys specified in order and deep values specified as a set.

    Parameters
    ----------
    fname: String
        The path to the text file containing the values
    keys: List
        Outer dictionary key, inner dictionary key
    values: List
        Inner dictionary values

    """
    cwd = os.getcwd()
    infile = open(cwd+'/Tests/Data/' + fname, mode='r')
    records = csv.reader(infile)
    header = next(records)
    key1_idx = header.index(keys[0])
    key2_idx = header.index(keys[1])
    values_ids = [header.index(value) for value in values]

    key1_to_key2_to_data = defaultdict(lambda: defaultdict(list))

    if dtype == 'int':
        for record in records:
            data = []
            for value_idx in values_ids:
                try:
                    data.append(int(record[value_idx]))
                except ValueError:
                    data.append(None)
            key1_to_key2_to_data[record[key1_idx]][record[key2_idx]].append(data)
    else:
        for record in records:
            data = []
            for value_idx in values_ids:
                try:
                    data.append(int(record[value_idx]))
                except ValueError:
                    data.append(None)
            key1_to_key2_to_data[record[key1_idx]][record[key2_idx]].append(data)

    return key1_to_key2_to_data
